from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from .serializer import UserSerializer
from .models import User


class UserView(CreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()

