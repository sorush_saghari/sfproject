from django.contrib import admin
from .models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    fields = ('email', 'password', 'is_active', 'is_staff', 'date_joined')
    list_display = ('email', 'is_active', 'is_staff', 'date_joined')
