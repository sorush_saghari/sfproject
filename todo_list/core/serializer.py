from rest_framework.serializers import ModelSerializer
from .models import User


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'date_joined', 'is_staff', 'password']
        extra_kwargs = {'password': {'write_only': True}}
        read_only_fields = ['is_staff', 'date_joined']

    def create(self, validated_data):
        user = User(**validated_data)
        user.set_password(validated_data.get('password'))
        user.save()
        return user
