from django.db import models
from django.contrib.postgres.fields import ArrayField
import datetime
from core.models import User
from .utils import CategoryTypes


class Category(models.Model):
    category_name = models.CharField('CategoryName', null=False, max_length=128)
    # category_id = models.ForeignKey('Cat_id',on_delete=models.CASCADE, null=False)
    # type = models.IntegerChoices(choices=CategoryTypes.choices(), default=CategoryTypes.TODO)

    # def get_category_type(self):
    #     return CategoryTypes(self.type).name.title()


class Todo(models.Model):
    due_date = models.DateTimeField('DueDate', null=False, default=datetime.datetime.now() + datetime.timedelta(1))
    description = models.CharField('Description', null=True, max_length=256)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    reminder = models.DateTimeField('Reminder', null=False)
    category_list = models.ManyToManyField(Category)
    tags = ArrayField(models.CharField(max_length=128), blank=True, null=True)
    done = models.BooleanField('Done', default=False)

    def save(self, *args, **kwargs):
        if not self.reminder:
            self.reminder = self.due_date.date() - datetime.timedelta(1)
        super(Todo, self).save(*args, **kwargs)
