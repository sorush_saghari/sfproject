from django.contrib import admin
from .models import Category, Todo


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    fields = ('category_name', )
    list_display = ('category_name', )


@admin.register(Todo)
class TodoAdmin(admin.ModelAdmin):
    fields = ('due_date', 'description', 'user', 'reminder', 'category_list')
    list_display = ('due_date', 'description', 'user', 'reminder', )
