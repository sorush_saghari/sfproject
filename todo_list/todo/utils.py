from enum import IntEnum

class CategoryTypes(IntEnum):
    TODO = 1
    DOING = 2
    DONE = 3

    @classmethod
    def choices(cls):
        return [(key.value, key.name) for key in cls]