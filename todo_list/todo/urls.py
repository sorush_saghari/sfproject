from django.urls import path
from .views import TodoView, CategoryView, CategoryTodoView

urlpatterns = [
    path('todo/', TodoView.as_view()),
    path('category/', CategoryView.as_view(), name='post'),
    path('cat/', CategoryTodoView.as_view())
]