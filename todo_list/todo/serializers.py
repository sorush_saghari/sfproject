from rest_framework.serializers import ModelSerializer, Serializer, CharField, ListField
from .models import Todo
from .models import Category


class TodoSerializer(ModelSerializer):
    class Meta:
        model = Todo
        fields = ('due_date', 'description', 'user', 'reminder', 'category_list', 'tags', 'done')
        read_only_fields = ('user',)

    def create(self, validated_data):
        categories = validated_data.get('category_list')
        del validated_data['category_list']
        todo = Todo(**validated_data)
        todo.save()
        todo.category_list.set(categories)
        return todo


# not sure about it - refactor it 2morrow
class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ['category_name', ]

    def create(self, validated_data):
        category = Category(**validated_data)
        category.save()
        return category


class CategoryTodoSerializer(Serializer):
    name = CharField()
    todos = ListField(child=TodoSerializer(), allow_empty=True)
