from django.shortcuts import render
from rest_framework.generics import CreateAPIView, ListAPIView, ListCreateAPIView
from .serializers import CategorySerializer, TodoSerializer, CategoryTodoSerializer
from .models import Category, Todo
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


class CategoryView(CreateAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class TodoView(ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = TodoSerializer

    def get_queryset(self):
        return Todo.objects.filter(user=self.request.user.pk)



class CategoryTodoView(ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = CategoryTodoSerializer
    queryset = Category.objects.all()

    def get_queryset(self):
        Category.objects.filter()

    def get(self, request, *args, **kwargs):
        ctegories = Category.objects.all()
        data = []

        for cat in ctegories:
            # print(dir(cat))
            # input()
            data.append({
                'name': cat.category_name,
                'todos': cat.todo_set.filter(user=request.user.pk)
            })

        return Response(CategoryTodoSerializer(data, many=True).data)
